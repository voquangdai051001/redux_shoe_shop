import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QUANTITY, DELETE_SHOE } from "./redux/constant/shoeConstant";
import { shoeReducer } from "./redux/reducer/shoeReducer";

class Cart_Shoe extends Component {
  renderCart = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>
            <img style={{ width: "80px" }} src={item.image}></img>
          </td>
          <td>{item.name}</td>
          <td style={{ textAlign: "center" }} className="p-0">
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, 1);
              }}
              style={{ width: "20px", padding: "8px 0" }}
              className="btn btn-success"
            >
              +
            </button>
            <strong className="px-2">{item.soLuong}</strong>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, -1);
              }}
              style={{ width: "20px", padding: "8px 0" }}
              className="btn btn-warning"
            >
              -
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDeleteShoe(item.id);
              }}
              className="btn text-danger border-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Image</th>
              <th>Name</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderCart()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDeleteShoe: (id) => {
      let action = {
        type: DELETE_SHOE,
        payload: id,
      };
      dispatch(action);
    },
    handleChangeQuantity: (id, luaChon) => {
      let action = {
        type: CHANGE_QUANTITY,
        payload: { id, luaChon },
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart_Shoe);
