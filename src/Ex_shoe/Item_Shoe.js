import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ADD_SHOE,
  DELETE_SHOE,
  DETAIL_SHOE,
} from "./redux/constant/shoeConstant";

class Item_Shoe extends Component {
  render() {
    let { name, price, image } = this.props.dataItem;
    return (
      <div className="col-6 py-3">
        <div className="card text-center pb-3">
          <img
            style={{ width: "200px" }}
            className="card-img-top"
            src={image}
          />
          <div className="card-body">
            <h5 style={{ fontSize: "16px" }} className="card-title">
              {name}
            </h5>
            <p className="card-text">{price}</p>
          </div>
          <div>
            <button
              onClick={() => {
                this.props.handleDetail(this.props.dataItem);
              }}
              className="btn btn-warning"
            >
              View Detail
            </button>
            <button
              onClick={() => {
                this.props.handleAddProduct(this.props.dataItem);
              }}
              className="btn btn-success"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddProduct: (itemShoe) => {
      let action = {
        type: ADD_SHOE,
        payload: itemShoe,
      };
      dispatch(action);
    },
    handleDetail: (shoe) => {
      let action = {
        type: DETAIL_SHOE,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(Item_Shoe);
