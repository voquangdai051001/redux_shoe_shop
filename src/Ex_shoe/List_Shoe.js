import React, { Component } from "react";
import { connect } from "react-redux";
import Item_Shoe from "./Item_Shoe";
import { shoeReducer } from "./redux/reducer/shoeReducer";

class List_Shoe extends Component {
  renderListShoe = () => {
    return this.props.list.map((item) => {
      return (
        <Item_Shoe
          dataItem={item}
          // handleViewDetail={this.props.handleViewDetail}
          // handleAddCart={this.props.handleAddCart}
        />
      );
    });
  };

  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}

let mapStateToProps = (state) => {
  return {
    list: state.shoeReducer.shoeArr,
  };
};

export default connect(mapStateToProps)(List_Shoe);
