export const ADD_SHOE = "ADD_SHOE";
export const DETAIL_SHOE = "DETAIL_SHOE";
export const CHANGE_QUANTITY = "CHANGE_QUANTITY";
export const DELETE_SHOE = "DELETE_SHOE";
