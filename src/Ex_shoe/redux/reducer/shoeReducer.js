import { shoeArr } from "../../Data";
import {
  ADD_SHOE,
  CHANGE_QUANTITY,
  DELETE_SHOE,
  DETAIL_SHOE,
} from "../constant/shoeConstant";

let initialState = {
  shoeArr: shoeArr,
  detailShoe: shoeArr[3],
  cart: [],
};

export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case DETAIL_SHOE: {
      // state.detailShoe = action.payload.shoe;
      return { ...state, detailShoe: action.payload };
    }
    case ADD_SHOE: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let newShoe = { ...action.payload, soLuong: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].soLuong++;
      }
      return { ...state, cart: cloneCart };
    }
    case DELETE_SHOE: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload;
      });
      cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    case CHANGE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      cloneCart[index].soLuong += action.payload.luaChon;
      cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    default: {
      return state;
    }
  }
};
